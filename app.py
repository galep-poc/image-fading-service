"""App module."""
from flask import Flask

app = Flask("Image fading service")

@app.route("/<string:filename>/<string:hexa_color>")
def fading(filename, hexa_color):
    return f'Filename {filename}, HexaColor {hexa_color}'
